module.exports = {
  content: ["./src/**/*.{html,js}"],
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    backgroundPosition: {
      top: "top",
      "top-right": "top 200px right",
    },
    colors: {
      green: "#8DCC7B",
      gray: "#828282",
    },
    fontSize: {
      xs: [
        "13px",
        {
          lineHeight: "15,25px",
        },
      ],
      sm: ".875rem",
      "sm-2": "14px",
      base: "16px",
      lg: "20px",
      xl: "1.25rem",
      "2xl": "1.5rem",
      "3xl": "32px",
      "4xl": "34px",
      "5xl": "3rem",
      "6xl": "4rem",
      "7xl": "5rem",
    },
    extend: {
      width: {
        "190": "190px",
        "390": "390px",
      },
      height: {
        "244": "244px",
        "260": "260px",
        "304": "304px",
        "462": "462px",
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
